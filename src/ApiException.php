<?php
namespace SurRealistik\ApiExceptions;


class ApiException extends \Exception
{
    /**
     * @param $msg
     * @param $code
     * @param \Exception|null $previous
     * @return static
     */
    public static function make($msg,$code,\Exception $previous = null){

        return new static($msg,$code,$previous);
    }

    /**
     * ApiException constructor.
     * @param string $message
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct($message, $code, \Exception $previous = null)
    {
        $message = is_array($message) ? json_encode($message) :

                    json_encode(['message'=>$message,'code'=>$code]);

        parent::__construct($message, $code, $previous);
    }

}
